# Requires GNU make, xargs, a latex distribution, sage
# and sagetex.sty visible in TEXINPUTS

MAINTEXFILE = main.tex
TEXFILES = ${MAINTEXFILE}
SAGETEXSCRIPT = main.sagetex.sage
SAGEPLOTFILES = sagetexscripts/*.sage
PYPLOTFILES = $(SAGEPLOTFILES:%.sage=%.py)

main.pdf: ${TEXFILES}  main.sagetex.sout
	latexmk

main.sagetex.sout: ${SAGETEXSCRIPT} ${PYPLOTFILES}
	PYTHONPATH=./sagetexscripts/ sage ${SAGETEXSCRIPT}

${SAGETEXSCRIPT}: ${TEXFILES}
	latexmk -interaction=nonstopmode ${MAINTEXFILE} || echo this shoud fail

pymodules: ${PYPLOTFILES}

${PYPLOTFILES}: ${SAGEPLOTFILES}
	# Preparse Sagemath scripts in sagetexscripts,
	# converting them to python files of form <basename>.py
	# (ready to be imported as python modules)
	./build_pymodules.sh

.PHONY: clean
clean:
	rm -rf **/__pycache__
	latexmk -C
	git clean -xf || echo no git repo to use for cleaning

