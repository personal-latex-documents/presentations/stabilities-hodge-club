#!/bin/sh

sage --preparse sagetexscripts/*.sage

for file in sagetexscripts/*.sage.py
do
	mv $file $( echo $file | sed -e 's;\.sage;;' )
done
