\documentclass[aspectratio=169]{beamer}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{sagetex}
\usepackage{ulem}
\usepackage{xcolor}
\usepackage{tcolorbox}

\usetheme{edmaths}
\renewcommand\mathfamilydefault{\rmdefault}

\title{Bridgeland Stabilities and Finding Walls}
\subtitle{}
\author{Luke Naylor}
\institute{Hodge Club}
\date{March 2023}

\newcommand\RR{\mathbb{R}}
\newcommand\CC{\mathbb{C}}
\newcommand\ZZ{\mathbb{Z}}
\newcommand\QQ{\mathbb{Q}}
\newcommand\centralcharge{\mathcal{Z}}
\newcommand\coh{\operatorname{Coh}}
\newcommand\rank{\operatorname{rk}}
\newcommand\degree{\operatorname{deg}}
\newcommand\realpart{\mathfrak{Re}}
\newcommand\imagpart{\mathfrak{Im}}
\newcommand\bigO{\mathcal{O}}
\newcommand\cohom{\mathcal{H}}
\newcommand\chern{\mathrm{ch}}
\newcommand\Torsion{\mathcal{T}}
\newcommand\Free{\mathcal{F}}
\newcommand\firsttilt[1]{\mathcal{B}^{#1}}
\newcommand\secondtilt[2]{\mathcal{A}^{#1,#2}}
\newcommand\derived{\mathcal{D}}

\begin{document}

\begin{sagesilent}
	from pseudowalls import *
	from sagetexscripts import *
\end{sagesilent}

\begin{frame}
	\titlepage
\end{frame}

\section{Transitioning to Stab on Triangulated Categories}

\begin{frame}{Central Charge for Mumford Stability}
	\begin{align*}
		&\centralcharge \colon \coh(X) \to \CC \\
		&\centralcharge (E) = - \degree(E) + i \rank(E)
	\end{align*}

	\vfill

	\resizebox{1\hsize}{!}{
		\sageplot{fig1.plot()}
	}

	\begin{columns}[T] % align columns
	\begin{column}{.48\linewidth}
		\[
			\centralcharge (E) = r(E) e^{i\pi \varphi(E)}
		\]
		\begin{center}
			\begin{large}
			$\varphi$ called "phase"
			\end{large}
		\end{center}
	\end{column}%
	%\hfill%
	\begin{column}{.48\linewidth}
		\[
			\mu(E) =
			\frac{
				- \realpart(\centralcharge(E))
			}{
				\imagpart(\centralcharge(E))
			}
			\quad
		\]
		\begin{center}
			\begin{large}
				(allow for $+\infty$)
			\end{large}
		\end{center}
	\end{column}%
	\end{columns}

\end{frame}

\begin{frame}{Extending Central Charge to $D^b(X)$}
	\[
		E^\bullet = [\cdots \to * \to * \to * \to \cdots] \in D^b(X)
	\]
	\begin{align*}
		\rank(E^\bullet) &= \sum (-1)^i \rank(\cohom^i(E)) \\
		\degree(E^\bullet) &= \sum (-1)^i \degree(\cohom^i(E))
	\end{align*}

	\vfill

	\begin{columns}[t,onlytextwidth]
		\begin{column}{.5\linewidth}
			In particular, for shifts:
			\begin{itemize}
				\item $\rank(E[1]) = - \rank(E)$
				\item $\degree(E[1]) = - \degree(E)$
			\end{itemize}
		\end{column}
		\begin{column}{.49\linewidth}
			For $\centralcharge$:
			\begin{itemize}
				\item $\centralcharge(E[1]) = - \centralcharge(E)$
				\item $\centralcharge(E[2]) = \centralcharge(E)$
			\end{itemize}
		\end{column}
	\end{columns}

\end{frame}

\begin{frame}{Slicing - Phase $\phi$}
\begin{columns}[t,onlytextwidth] % align columns
	\begin{column}{.33\linewidth}%
	\resizebox{1.1\hsize}{!}{
		\sageplot{fig2.plot()}
	}
	\end{column}%
	%\hfill%
	\begin{column}{.33\linewidth}%
	\resizebox{1.1\hsize}{!}{
		\sageplot{fig3.plot()}
	}
	\end{column}%
	%\hfill%
	\begin{column}{.33\linewidth}%
	\resizebox{1.1\hsize}{!}{
		\sageplot{fig4.plot()}
	}
	\end{column}%
	\end{columns}

	\vfill

	\begin{itemize}
		\item $\centralcharge(E[2]) = \centralcharge(E)$
		\item But $\phi(E[n]) = \phi(E) + n$
		\item Stability decided among $E \in D^b(X) \colon \phi(E) \in (0, \pi] =
			\coh(X)$ \\
			($\phi$ only partial ``function'')
	\end{itemize}
\end{frame}

\begin{frame}{Benefits of this Generalization}
	\begin{itemize}
		\item Can take different slicings (and heart)
		\item Tweak $\centralcharge$ \quad $\to$ \quad m.b. tweak slicing
		\item No ``strong'' Bridgeland stabilities with $\coh(X)$ as heart for dim>1
		\item Gieseker stability (a polynomial stability) can be constructed as a
			limit of Bridgeland stabilities
	\end{itemize}
\end{frame}

\section{Moving to Picard Rank 1 Surfaces}

\begin{frame}{Moving to Surfaces}
	\begin{itemize}
		\item $\centralcharge(\bigO_x) = 0$ for Mumford stability
			\\ ignores extra term in Chern character:
			$(r, d \ell, \chi)$
		\item Classically, Gieseker stability used
			\\ \qquad slope comparison $\to$ lexicographic comparison
	\end{itemize}
\end{frame}

\begin{frame}{New Central Charges for Surfaces}
	Explicitly constructed for K3 - Bridgeland (2003)
	\vfill

	\begin{tcolorbox}[title=Picard Rank 1 with polarization $L$]
		\begin{align*}
			\centralcharge_{\alpha, \beta}(E) &:=
			- \left<
				\exp( - \beta \ell - \alpha \ell i),
				E
			\right>
																				&\text{where}\:\:\ell := c_1(L),\:
																				\alpha\in\RR^{>0},\beta\in\RR
			\\ &= - \chern_{\mathrm{top}}(\exp( \alpha \ell + \beta \ell i)^{-1} \otimes E)
																				&\text{($\leftarrow$ abuse)}
		\end{align*}
	\end{tcolorbox}
	\vfill
	$\exp(a) = \left(1, a, \frac12 a^2\right)$ defined formally,
	in particular: $\exp(n \ell) = L^{\otimes n}$
	\vfill
	{
		\color{gray}
		For Mumford stability on Curves:
		\begin{align*}
			\centralcharge(E) &= -\chern_1(E) + \chern_0(E) i
										 \\ &= - \left<\exp(-i), E\right>
		\end{align*}
	}
\end{frame}

\begin{sagesilent}
	v = generic_chern_char(2, "v")
	Z = stability.Tilt().central_charge(v).expand()
	nu = stability.Tilt().slope(v)
\end{sagesilent}

\begin{frame}{Explicit Formulae for New Central Charge}
	\begin{align*}
		\centralcharge_{\alpha, \beta}\left(v_0, v_1 \ell, v_2 \ell^2\right)
		&= \sage{Z} \\
		\nu_{\alpha, \beta}\left(v_0, v_1 \ell, v_2 \ell^2\right)
		&= \sage{nu} \\
	\end{align*}
	\vfill
	\begin{center}
		Denominator /
	$\imagpart(\centralcharge_{\alpha, \beta}) > 0 \iff \beta < \frac{v_1}{v_0} = \mu$
	\\ $\to$ other $E\in\coh(X)$ cannot be in heart
	\end{center}
\end{frame}

\begin{frame}{New Heart - Tilting}
	Role of $\coh(X)$ as heart of $\derived^b(X)$ replaced:
	\vfill
	\begin{tcolorbox}[title=First Tilt of $\coh(X)$]
		\begin{align*}
			\firsttilt\beta :=
			\left\{
				E \in \derived^b(X) \colon \quad
				\cohom^{0}(E) \in \Torsion_\beta, \quad
				\cohom^{-1}(E) \in \Free_\beta, \quad
				\cohom^i(E) = 0 \:\: \text{o.w.}
			\right\}
		\end{align*}
		where $\beta \in \RR$ and:
		\begin{align*}
			\Tor \deribeta &:=
			\left\{\:
				E \in \coh(X) \colon \qquad
				\mu(G) > \beta \quad \text{whenever} \: E \twoheadrightarrow G \not=0,E
			\:\right\}&&
			{\color{gray}
				\: \ni \cohom^0
			}
		\\
			\Free_\beta &:=
			\left\{\:
				E \in \coh(X) \colon \qquad
				\mu(G) \leq \beta \quad \text{whenever} \: 0 \not= G \hookrightarrow E
			\:\right\}&&
			{\color{gray}
				\: \ni \cohom^{-1}
			}
		\end{align*}
	\end{tcolorbox}
	\vfill
	\begin{itemize}
		\item $\Torsion_\beta \subset \firsttilt\beta$ includes Mumford semistable
			$E \in Coh(X)$ s.t. $\mu(E) \geq \beta$
		\item As $\beta \to - \infty$, \: $\firsttilt\beta \rightsquigarrow \coh(X)$
			\begin{itemize}
				\item $\Torsion_\beta \rightsquigarrow \coh(X)$
				\item $\Free_\beta \rightsquigarrow 0$
			\end{itemize}
		{\color{gray}
		\item $\hom(T, F) = 0$ for $T \in \Torsion_\beta, F \in \Free_\beta$
			makes this a torsion theory
		\item As $\beta \to +\infty$, \:
			$\Torsion_\beta \rightsquigarrow$ torsion sheaves (includes skyscrapers)
		}
	\end{itemize}
\end{frame}

\begin{frame}{Tilts $\firsttilt\beta$ on $\alpha,\beta$-Plane}
	When $E \in \coh(X)$ is Gieseker semistable (hence Mumford semistable):
	\resizebox{1\hsize}{!}{
		\sageplot{fig5.plot()}
	}
\end{frame}

\begin{frame}{Notable Stability Conditions on Plane}
	\begin{columns}[t,onlytextwidth] % align columns
		\begin{column}{.49\linewidth}
			$\beta \to -\infty$, Fixed $\alpha$
			\hline \vspace{1em}
			$n \in \ZZ$
			\begin{equation*}
				\nu_{\alpha, -n}(E) =
				\frac{
					\chern_2(E\otimes L^n)
					{\color{gray}
					- \frac{\alpha^2}{2} \rank(E)
					}
				}{
					{\color{gray}
					(\chern_1(E) +
					}
					n \rank(E)
					{\color{gray}
					)
					}
				}
				%n \in \ZZ
			\end{equation*}
			$\rightsquigarrow$ Tends to Gieseker Stability
			\vfill
			\begin{tcolorbox}[title=Gieseker Stability]
				E stable when red. Hilb. poly.
				\[
					p_E(n) = \frac{\chern_2(E\otimes L^n)}{\rank(E)}
				\]
				not overtaken by $p_F(n)$ of any \\
				$0 \not= F \hookrightarrow E$, for large $n$. \\
				{\color{gray}
				(equiv. to lexic. comparison between poly. coeffs)
				}
			\end{tcolorbox}
		\end{column}%
		\hfill%
		\begin{column}{.49\linewidth}
			$\beta = \mu(E)$
			\hline \vspace{1em}
			$\nu_{\alpha, \beta}(E) = + \infty$ \\
			so can only be destabilized by
			$F \hookrightarrow E$ with $\nu_{\alpha, \beta}(F) = + \infty$ too
			($\beta = \mu(F)$) \\
			$\rightsquigarrow$ Destabilized w.r.t. Mumford slope
		\end{column}%
	\end{columns}
\end{frame}

\section{Walls}
% walls, make the explanation about fixing a Chern character
\begin{frame}{Walls}
	Fix a Chern character $v$
	\begin{tcolorbox}[title=Wall point for $v$]
		Stability condition $\sigma$ where there's a strictly semistable object of
		Chern character $v$
	\end{tcolorbox}
	\vfill
	$\sigma_{\alpha, \beta}$ wall point for $v$:
	\hline \vspace{0.5em}
	$F \hookrightarrow E$,
	destabilizing in $\firsttilt\beta$, with $\chern(E) = v$:
	\begin{equation*}
		\nu_{\alpha, \beta}(E)
		=
		\nu_{\alpha, \beta}(F)
	\end{equation*}
	\begin{equation*}
		\text{equiv.:}\quad
		\realpart(\centralcharge_{\alpha,\beta}(E))
		\imagpart(\centralcharge_{\alpha,\beta}(F))
		-
		\imagpart(\centralcharge_{\alpha,\beta}(E))
		\realpart(\centralcharge_{\alpha,\beta}(F))
		= 0
	\end{equation*}
	\vfill
	\begin{itemize}
		\item $E$ strictly semistable
		\item $F$ necessarily semistable
		\item Polynomial condition on $\alpha, \beta$ (for fixed $E$)
		\item $E$ switches between stable and unstable crossing wall
		\item For $\sigma_{\alpha, \beta}$: concentric circles on
			either side of $\beta = \mu(v)$ centered on $\beta$-axis
	\end{itemize}
\end{frame}

\begin{frame}{Purpose of walls}
	MMP?
\end{frame}

\begin{frame}{Characteristic Curves for Tilt Stability}
	\resizebox{\hsize}{!}{ \sageplot{fig6.plot()} }
	\begin{itemize}
		\item Circular walls nested \& non-intersecting
		\begin{itemize}
			\item Circular walls apex on dotted hyperbola
				$\left(
					\Leftarrow
					\frac{\delta}{\delta \beta}
					\realpart(\centralcharge_{\alpha, \beta})
					= \imagpart(\centralcharge_{\alpha, \beta})
				\right)$
			\item Dotted hyperbola assymptotes angled $\pm \frac{\pi}{4}$ crossing
				$\beta$-axis at $\beta = \mu$
		\end{itemize}
	\item Always left-right branched hyperbolae, instead of up-down
		($\Leftarrow$ BG ineq)
	\end{itemize}
\end{frame}

\begin{frame}{Real Tilt Wall Example}
	fix v, example with many walls [...]
\end{frame}

\begin{frame}{Finitely Many walls for $\beta^{-} \in \QQ$}
	Twisted Chern reasoning
\end{frame}


\begin{frame}{Moving to Threefolds}
	Current $\centralcharge_{\alpha,\beta}$ has kernel in $\firsttilt\beta$ when
	defined on threefolds \\
	Ignores $\chern_3$, so can't see $\bigO_x$
	\vfill
	\begin{align*}
		\lambda_{\alpha, \beta, s}\left(E\right)
		&=
		\frac{
			\chern_3^\beta(E)
			- \alpha^2\left(s+\frac16\right)\chern_1^\beta(E)
		}{
			\chern_2^\beta(E)
			- \frac{\alpha^2}{2} \chern_0^\beta(E)
		}
	\end{align*}
	\vfill
	\begin{itemize}
		\item Denominator same as tilt slope ($\nu_{\alpha,\beta}$) numerator
		\item Create heart $\secondtilt\alpha\beta$ by tilting $\firsttilt\beta$ again, using $\nu_{\alpha,\beta}$
			\\ \qquad (same way $\firsttilt\beta$ used $\mu$ to tilt $\coh(X)$)
		
	\end{itemize}
\end{frame}

\begin{frame}
	\resizebox{\hsize}{!}{ \sageplot{fig7.plot()} }
\end{frame}

\begin{frame}
	\resizebox{\linewidth}{!}{ \sageplot{fig8.plot()} }
\end{frame}

\end{document}
