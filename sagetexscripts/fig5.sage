def plot():
	ymin = 0
	ymax = 2
	xmin = -3
	xmax = 1

	mu = -1

	p = line(
		[(mu,0), (mu,ymax)],
		linestyle = "dashed",
	) + text(
r"""$\beta = \mu(E)$
$\mathfrak{Im}(\mathcal{Z}_{\alpha,\beta}(E)) = 0$""",
		(mu, -0.25),
		fontsize = "large",
		rgbcolor = "black"
	) + text(
r"""$-\infty \longleftarrow \beta$
$\mathrm{Coh}(X) \leftarrow \mathcal{B}^\beta$""",
		(-2.5,1),
		fontsize = "large"
	) + text(
r"""$E \in \mathcal{B}^\beta$
$\mathfrak{Im}(\mathcal{Z}_{\alpha,\beta}(E)) \geq 0$""",
		(-1.5,1),
		fontsize = "large"
	) + text(
r"""$E[1] \in \mathcal{B}^\beta$
$\mathfrak{Im}(\mathcal{Z}_{\alpha,\beta}(E)) < 0$""",
		(-0.5,1),
		fontsize = "large",
		rgbcolor = "red"
	) + polygon(
		[
			(mu,0),
			(xmin,0),
			(xmin,ymax),
			(mu,ymax)
		],
		rgbcolor = "blue",
		alpha = 0.1,
		zorder = 102
	) + polygon(
		[
			(xmax,0),
			(mu,0),
			(mu,ymax),
			(xmax,ymax)
		],
		rgbcolor = "orange",
		alpha = 0.1,
		zorder = 102
	)

	p.axes_labels([r"$\beta$",r"$\alpha$"])
	p.tick_label_color("white")
	return p
