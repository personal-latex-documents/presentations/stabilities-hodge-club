from pseudowalls import *

def plot_central_charge(chern, name, argument = None, radius = None):
	Z = stability.Mumford().central_charge(chern)
	x = Z.real()
	y = Z.imag()

	if not argument:
		argument = arctan(y/x) if x != 0 else pi / 2
		if argument <= 0:
			argument += pi

	if not radius:
		radius = (pi-argument/2)/pi

	return point(
		Z,
		marker = "o",
		size = 800,
		rgbcolor = "white",
		#markeredgecolor = "purple",
		zorder = 100
	) + point(
		Z,
		marker = name,
		size = 700,
		rgbcolor = "red",
		zorder = 101
	) + line(
		(0, Z),
		rgbcolor = "red",
		linestyle = "dashed",
		zorder = 99
	) + disk(
		(0,0),
		float(radius),
		(0, float(argument)),
		alpha=.2,
		fill=False,
		thickness=1,
		rgbcolor="purple"
	)
