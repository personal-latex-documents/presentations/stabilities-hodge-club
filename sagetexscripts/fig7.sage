from pseudowalls import *

def plot():
	plot_range = 4
	v = Chern_Char(2,0,-1,1/2)
	lam = stability.Lambda(s=1/3)
	Theta = lam.rank(v)
	Gamma = lam.degree(v)
	mu = stability.Mumford().slope(v)
	p = (
	implicit_plot(Theta, (lam.beta, -plot_range,plot_range), (lam.alpha, 0, plot_range))
	+ implicit_plot(Gamma, (lam.beta, -plot_range,plot_range), (lam.alpha, 0, plot_range), rgbcolor="green")
	+ line([(mu,0),(mu,plot_range)], rgbcolor = "red")
	+ text(r"$\beta=\mu$", (mu, -0.5), fontsize="x-large", rgbcolor="red")
	+ text(r"$\nu = 0$", (mu+1, -0.5), fontsize="x-large", rgbcolor="blue")
	+ text(r"$\lambda = 0$", (mu-1.5, -0.5), fontsize="x-large", rgbcolor="green")
	)
	p.tick_label_color("white")
	p.ymin(0)
	p
	return p
