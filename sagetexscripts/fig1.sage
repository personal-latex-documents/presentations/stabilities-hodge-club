#!/usr/bin/env sage

from pseudowalls import *
from .plot_utils import plot_central_charge

def plot():
    O = Chern_Char(1,0)
    O1 = exponential_chern(1,1)
    O1inv = exponential_chern(-1,1)
    O2 = O1 * O1
    O_x = Chern_Char(0,1)


    p = sum(
        plot_central_charge(chern, name)
        for chern, name in [
            (O1, r"$\mathcal{Z}(\mathcal{O}(1))$"),
            (O, r"$\mathcal{Z}(\mathcal{O}_X)$"),
            (O1inv, r"$\mathcal{Z}(\mathcal{O}(-1))$"),
            (O2, r"$\mathcal{Z}(\mathcal{O}(2))$"),
            (O_x, r"$\mathcal{Z}(\mathcal{O}_p)$")
        ]
    )

    xmax = (2.5)
    xmin = (-2.5)
    ymin = (-0.25)
    ymax = (1.5)
    aspect_ratio = (1)

    p += polygon(
        [
            (xmax + 1,0),
            (xmin - 1,0),
            (xmin - 1,ymax + 1),
            (xmax + 1,ymax + 1)
        ],
        rgbcolor = "yellow",
        alpha = 0.2,
        zorder = 102
    )

    p.xmax(xmax)
    p.xmin(xmin)
    p.ymin(ymin)
    p.ymax(ymax)
    p.set_aspect_ratio(aspect_ratio)

    p.axes_labels([r"$\mathcal{R}$",r"$\mathcal{I}$"])
    return p
