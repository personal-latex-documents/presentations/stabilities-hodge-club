from pseudowalls import *
from .plot_utils import plot_central_charge

def plot():
	O = Chern_Char(1,0,0)
	p = plot_central_charge(O, r"$\mathcal{O}_X$", radius = 0.9)
	p += text(r"$\phi = \frac{1}{2}$" , (0.5, 0.5), rgbcolor="purple", fontsize="xx-large")
	p.ymax(1.2)
	p.ymin(-1.2)
	p.xmax(1.2)
	p.xmin(-1.2)
	return p
