from pseudowalls import *

def plot():
	v = Chern_Char(1,0,-1)
	u = Chern_Char(1,1,-3)

	ts = stability.Tilt()
	mu = stability.Mumford().slope

	p = implicit_plot(
		ts.wall_eqn(u,v)/ts.alpha == 0,
		(ts.beta, -5, 5),
		(ts.alpha, 0, 5),
		rgbcolor = "purple",
	) + implicit_plot(
		ts.degree(v) == 0,
		(ts.beta, -5, 5),
		(ts.alpha, 0, 5),
		rgbcolor = "red",
		linestyle = "dotted",
	) + implicit_plot(
		ts.degree(u) == 0,
		(ts.beta, -5, 5),
		(ts.alpha, 0, 5),
		linestyle = "dotted",
	) + line(
		[(mu(v),0),
		(mu(v),5)],
		rgbcolor = "red",
		legend_label = "Potential destabilizer",
	) + line(
		[(mu(u),0),
		(mu(u),5)],
		legend_label = "Fixed $v$ who's repr. obj. destabilized",
	)

	p.tick_label_color("white")
	return p
