from pseudowalls import *
from .plot_utils import plot_central_charge

def plot():
	O = Chern_Char(1,0,0)
	p = plot_central_charge(O, r"$\mathcal{O}_X[2]$", argument=2*pi, radius=0.9)

	p += disk(
		(0,0),
		float(0.9),
		(0, float(5*pi/2)),
		alpha=.6,
		fill=False,
		thickness=1,
		rgbcolor="purple"
	)
	p += text(r"$\phi = \frac{5}{2}$" , (0.5, 0.5), rgbcolor="purple", fontsize="xx-large")
	p.ymax(1.2)
	p.ymin(-1.2)
	p.xmax(1.2)
	p.xmin(-1.2)
	return p
